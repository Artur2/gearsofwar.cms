﻿using System;
using System.Linq;
using System.Reflection;
using System.Web.Helpers;
using System.Web.Mvc;
using GearsOfWar.Cms.Data;
using GearsOfWar.Cms.Data.Query.Extensions;
using GearsOfWar.Cms.Helpers;
using Microsoft.Practices.Unity;

namespace GearsOfWar.Cms.Controllers
{
    public class EntityController : Controller
    {
        //TODO: Get type from AppDomain
        public Type Type { get; set; }

        [Dependency]
        public IUnitOfWork UnitOfWork { get; set; }

        [Dependency]
        public ReflectionHelper ReflectionHelper { get; set; }

        // GET: Entity
        public ActionResult List(int page = 1)
        {
            var items = UnitOfWork.Items(Type)
                .OrderBy("Id", SortDirection.Ascending)
                .Skip(page > 1 ? page * 20 : 0)
                .Take(20);

            return View(items);
        }

        [HttpGet]
        public ActionResult Create()
        {
            var model = Activator.CreateInstance(Type);

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(FormCollection collection, string returnUrl)
        {
            var model = Activator.CreateInstance(Type);

            TryUpdateModelWithType(model);

            if (!ModelState.IsValid)
                return View(model);

            try
            {
                UnitOfWork.Add(model);
                UnitOfWork.Save();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex);
                return View(model);
            }

            return Redirect(returnUrl);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = UnitOfWork.Get(id, Type);
            if (model == null)
                return HttpNotFound();

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(FormCollection collection, int id, string returnUrl)
        {
            var model = UnitOfWork.Get(id, Type);
            if (model == null)
                return HttpNotFound();

            TryUpdateModelWithType(model);
            if (!ModelState.IsValid)
                return View(model);

            try
            {
                UnitOfWork.Save();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, ex);
                return View(model);
            }

            return Redirect(returnUrl);
        }

        [HttpGet]
        public ActionResult Delete(int id, string returnUrl)
        {
            var model = UnitOfWork.Get(id, Type);
            if (model == null)
                return HttpNotFound();
            
            UnitOfWork.Remove(model);
            UnitOfWork.Save();

            return Redirect(returnUrl);
        }

        private bool TryUpdateModelWithType(
            object model,
            string prefix = null,
            string[] includeProperties = null,
            string[] excludeProperties = null,
            IValueProvider valueProvider = null)
        {
            if (model == null)
                return false;

            var methodInfo = GetType()
                .GetMethods(BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.NonPublic)
                .FirstOrDefault(x => x.Name == "TryUpdateModel" && x.GetParameters().Count() == 5)
                .MakeGenericMethod(new[] { Type });

            return (bool)methodInfo.Invoke(this, new[] {
                model, prefix, includeProperties, excludeProperties, valueProvider ?? ValueProvider
            });
        }
    }
}