﻿using System;
using System.Linq;

namespace GearsOfWar.Cms.Data
{
    public interface IUnitOfWork
    {
        T Get<T>(object id) where T : class;

        object Get(object id, Type type);

        void Add<T>(T entity) where T : class;

        void Add(object entity);

        void Remove<T>(T entity) where T : class;

        void Remove(object entity);

        void Save();

        IQueryable<T> Items<T>() where T : class;

        IQueryable Items(Type type);
    }
}