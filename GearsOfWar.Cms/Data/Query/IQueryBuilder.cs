﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GearsOfWar.Cms.Data.Query
{
    public interface IQueryBuilder
    {
        IQueryable Build(IQueryable queryable, IDictionary<string, object> @params);
    }
}
