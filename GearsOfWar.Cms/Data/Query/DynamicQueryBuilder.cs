﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace GearsOfWar.Cms.Data.Query
{
    /// <summary>
    /// Provides dynamic filtration for simple properties
    /// </summary>
    public class DynamicFilterQueryBuilder : IQueryBuilder
    {
        public DynamicFilterQueryBuilder()
        {

        }

        public const string FromPrefix = "from-", ToPrefix = "to-";

        private static readonly MethodInfo whereGeneric = typeof(DynamicFilterQueryBuilder).GetMethods(BindingFlags.Instance | BindingFlags.NonPublic)
            .FirstOrDefault(o => o.Name == "Where" && o.IsGenericMethodDefinition);

        //TODO: refact
        //UNDONE: Add visitor pattern
        private IQueryable Where<T>(IQueryable queryable, string name, object value)
        {
            var clearName = name.Replace(FromPrefix, string.Empty).Replace(ToPrefix, string.Empty);
            var entityType = typeof(T);

            var valueExpression = Expression.Constant(value);
            var parameterExpression = Expression.Parameter(entityType, "o"); // o
            var propertyExpression = Expression.Property(parameterExpression, clearName); // o.Property
            var binaryExpression = (BinaryExpression)null;

            if (name.StartsWith(FromPrefix))
            {
                binaryExpression = Expression.GreaterThanOrEqual(propertyExpression, valueExpression); // o.Property >= value
            }
            else if (name.StartsWith(ToPrefix))
            {
                binaryExpression = Expression.LessThanOrEqual(propertyExpression, valueExpression); // o.Property <= value
            }
            else
            {
                binaryExpression = Expression.Equal(propertyExpression, valueExpression); // o.Property == value
            }

            var lambda = Expression.Lambda<Func<T, bool>>(binaryExpression, parameterExpression); // o => o.Propery binOp value
            var call = Expression.Call(typeof(Queryable), "Where", new Type[] { queryable.ElementType }, queryable.Expression, lambda);

            return queryable.Provider.CreateQuery(call);
        }

        /// <summary>
        /// Executes filtering
        /// </summary>
        /// <returns>"Builded" query</returns>
        public IQueryable Build(IQueryable queryable, IDictionary<string, object> @params)
        {
            foreach (var keyValue in @params)
            {
                var methodToInvoke = whereGeneric.MakeGenericMethod(queryable.ElementType);

                queryable = methodToInvoke.Invoke(this, new[] { queryable, keyValue.Key, keyValue.Value }) as IQueryable;
            }

            return queryable;
        }
    }
}