﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Helpers;
using GearsOfWar.Cms.Helpers;

namespace GearsOfWar.Cms.Data.Query.Extensions
{
    [Obsolete]
    public static class QueryableExtensions
    {
        public static IQueryable OrderBy(this IQueryable @this, string propertyName, SortDirection direction)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
                throw new ArgumentNullException("propertyName");

            var reflectionHelper = new ReflectionHelper();
            var property = reflectionHelper.GetProperties(@this.ElementType, x => x.Name == propertyName)
                .FirstOrDefault();

            var parameterExpression = Expression.Parameter(@this.ElementType, "o"); // o
            var propertyExpression = Expression.Property(parameterExpression, propertyName); // o.Property

            var lambda = Expression.Lambda(propertyExpression, parameterExpression);

            var orderMethodName = direction == SortDirection.Ascending ? "OrderBy" : "OrderByDescending";

            return @this.Provider.CreateQuery(Expression.Call(typeof(Queryable), orderMethodName, new Type[] { @this.ElementType, property.PropertyType }, @this.Expression, lambda));
        }

        public static IQueryable Skip(this IQueryable @this, int count)
        {
            return @this.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Skip", new Type[] { @this.ElementType }, @this.Expression, Expression.Constant(count)));
        }

        public static IQueryable Take(this IQueryable @this, int count)
        {
            return @this.Provider.CreateQuery(Expression.Call(typeof(Queryable), "Take", new Type[] { @this.ElementType }, @this.Expression, Expression.Constant(count)));
        }
    }
}