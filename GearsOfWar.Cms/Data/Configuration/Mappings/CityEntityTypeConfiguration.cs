﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using GearsOfWar.Cms.Data.Entities;

namespace GearsOfWar.Cms.Data.Configuration.Mappings
{
    public class CityEntityTypeConfiguration : EntityTypeConfiguration<City>
    {
        public CityEntityTypeConfiguration()
        {
            //TODO: Добавить маппинги для остальных
        }
    }
}