﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;

namespace GearsOfWar.Cms.Data.Configuration
{
    public class MigrateToLatestVersionConfiguration : DbMigrationsConfiguration<DataContext>
    {
        public MigrateToLatestVersionConfiguration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = false;
        }
    }
}