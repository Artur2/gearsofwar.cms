﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using GearsOfWar.Cms.Data.Entities;
using GearsOfWar.Cms.Data.Query;

namespace GearsOfWar.Cms.Data
{
    public class DataContext : DbContext, IUnitOfWork
    {
        #region DbSets

        public IDbSet<City> Cities => Set<City>();

        public IDbSet<Weapon> Weapons => Set<Weapon>();

        public IDbSet<CogTag> CogTags => Set<CogTag>();

        public IDbSet<Gear> Gears => Set<Gear>();

        public IDbSet<Squad> Squads => Set<Squad>();

        #endregion

        #region IUnitOfWork Members

        public IQueryable<T> Items<T>() where T : class => Set<T>();

        public IQueryable Items(Type type) => Set(type);

        public void Add<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public void Add(object entity)
        {
            throw new NotImplementedException();
        }

        public void Remove<T>(T entity) where T : class
        {
            throw new NotImplementedException();
        }

        public void Remove(object entity)
        {
            throw new NotImplementedException();
        }

        public void Save() => this.SaveChanges();

        public T Get<T>(object id) where T : class => Set<T>()
            .Find(id);

        public object Get(object id, Type type) => Set(type)
            .Find(id);

        #endregion
    }
}