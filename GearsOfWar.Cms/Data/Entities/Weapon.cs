﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GearsOfWar.Cms.Data.Entities
{
    /// <summary>
    /// Оружие
    /// </summary>
    public class Weapon
    {
        public int Id { get; set; }

        public string Name { get; set; }
        
        public int? SynergyWithId { get; set; }

        public virtual Weapon SynergyWith { get; set; }

        public string OwnerNickname { get; set; }

        public int OwnerSquadId { get; set; }

        public virtual Squad OwnerSquad { get; set; }

        public int OwnerId { get; set; }

        public virtual Gear Owner { get; set; }
    }
}