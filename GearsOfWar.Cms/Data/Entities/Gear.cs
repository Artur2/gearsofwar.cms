﻿using System.Collections.Generic;

namespace GearsOfWar.Cms.Data.Entities
{
    //TODO: configure fluently

    /// <summary>
    /// Солдат
    /// </summary>
    public class Gear
    {
        public int Id { get; set; }

        public string Nickname { get; set; }

        public string FullName { get; set; }

        public string CityOrBirthName { get; set; }

        public int CityOfBirthId { get; set; }

        public virtual City CityOfBirth { get; set; }

        public int AssignedCityId { get; set; }

        public virtual City AssignedCity { get; set; }

        public MilitaryRank Rank { get; set; }

        public int TagId { get; set; }

        public virtual CogTag Tag { get; set; }

        public int SquadId { get; set; }

        public virtual Squad Squad { get; set; }

        public virtual ICollection<Weapon> Weapons { get; set; }

        public string LeaderNickname { get; set; }

        public int LeaderSquadId { get; set; }

        public virtual Squad LeaderSquad { get; set; }

        public virtual ICollection<Gear> Reports { get; set; }
    }
}