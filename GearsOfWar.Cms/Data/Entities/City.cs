﻿using System.Collections.Generic;

namespace GearsOfWar.Cms.Data.Entities
{
    /// <summary>
    /// Город
    /// </summary>
    public class City
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public virtual ICollection<Gear> BornGears { get; set; }

        public virtual ICollection<Gear> StationedGears { get; set; }
    }
}