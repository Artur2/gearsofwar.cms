﻿using System.Collections.Generic;

namespace GearsOfWar.Cms.Data.Entities
{
    /// <summary>
    /// Жетон
    /// </summary>
    public class CogTag
    {
        public int Id { get; set; }

        public string Note { get; set; }

        public virtual ICollection<Gear> Gears { get; set; }
    }
}