﻿using System.Collections.Generic;

namespace GearsOfWar.Cms.Data.Entities
{
    /// <summary>
    /// Отряд
    /// </summary>
    public class Squad
    {
        public int Id { get; set; }

        public string Name { get; set; }
        // auto-generated non-key (sequence)

        public int InternalNumber { get; set; }

        public virtual ICollection<Gear> Members { get; set; }
    }
}