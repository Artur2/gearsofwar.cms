﻿namespace GearsOfWar.Cms.Data.Entities
{
    /// <summary>
    /// Ранг
    /// </summary>
    public enum MilitaryRank
    {
        None = 0,
        Private,
        Corporal,
        Sergeant,
        Lieutenant,
        Captain,
        Major,
        Colonel,
        General
    }
}