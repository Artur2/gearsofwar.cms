﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace GearsOfWar.Cms
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("Entity_List",
                url: "cms/{Type}/{page}/{Collection}",
                defaults: new { controller = "Entity", action = "List", Collection = UrlParameter.Optional, page = UrlParameter.Optional }
                );

            routes.MapRoute("Entity_Create",
                url: "cms/{Type}/Create",
                defaults: new { controller = "Entity", action = "Create" }
                );

            routes.MapRoute("Entity_Edit",
                url: "cms/{Type}/Edit/{id}",
                defaults: new { controller = "Entity", action = "Edit" }
                );

            routes.MapRoute("Entity_Delete",
                url: "cms/{Type}/Delete/{id}",
                defaults: new { controller = "Entity", action = "Delete" }
                );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
