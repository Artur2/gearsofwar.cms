using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using GearsOfWar.Cms.Helpers;
using GearsOfWar.Cms.Data;
using GearsOfWar.Cms.Data.Query;

namespace GearsOfWar.Cms
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            container.RegisterType<DataContext, DataContext>();
            container.RegisterType<IUnitOfWork, DataContext>();
            container.RegisterType<IQueryBuilder, DynamicFilterQueryBuilder>();
            container.RegisterType<ReflectionHelper, ReflectionHelper>();

            // e.g. container.RegisterType<ITestService, TestService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}