﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace GearsOfWar.Cms.Helpers
{
    public class ReflectionHelper
    {
        public IEnumerable<PropertyDescriptor> GetProperties(Type type, Predicate<PropertyDescriptor> predicate = null)
        {
            foreach (var propertyDescriptor in TypeDescriptor.GetProperties(type)
                .OfType<PropertyDescriptor>())
            {
                if (predicate != null && !predicate(propertyDescriptor))
                    continue;

                yield return propertyDescriptor;
            }
        }
    }
}